package az.ingress.ms10security.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Configuration
@RequiredArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final PasswordEncoder passwordEncoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("/hello/public")
                .permitAll()
                .antMatchers("/hello/developer")
                .hasAnyRole("developer")
                .antMatchers("/hello/admin")
                .hasAnyRole("admin", "ceo")
                .antMatchers(HttpMethod.GET, "/hello/books")
                .hasAnyRole("developer", "admin", "ceo")
                .antMatchers(HttpMethod.POST, "/hello/books")
                .hasAnyRole("admin");

        super.configure(http);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("orxan")
                .password(passwordEncoder.encode("1234"))
                .roles("admin", "ceo")
                .and()
                .withUser("elgun")
                .password((passwordEncoder.encode("1234")))
                .roles("developer");
    }
}
